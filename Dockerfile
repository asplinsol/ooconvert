FROM node:16-slim

WORKDIR /usr/src/app

RUN apt-get update && apt-get upgrade -y && \
  apt install -y \
    libreoffice-core \
    unoconv \
    hyphen-en-us \
    fonts-dejavu fonts-dejavu-core fonts-dejavu-extra \
    fonts-droid-fallback fonts-dustin fonts-f500 fonts-fanwood fonts-freefont-ttf fonts-liberation \
    fonts-lmodern fonts-lyx fonts-sil-gentium fonts-texgyre fonts-tlwg-purisa fonts-opensymbol && \
  rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY . .
RUN npm ci && npm run build && npm run test