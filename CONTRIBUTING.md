# Contributing

First of all contributions are very much welcome!


## How to contribute

* Check if there is an existing discussion relevent in GitLab under [Issues](https://gitlab.com/asplinsol/ooconvert/issues).

* If you're unable to find an open issue addressing the subject, [open a new one](https://gitlab.com/asplinsol/ooconvert/issues/new). Be sure to include a **title and clear description**, as much relevant information as possible, and a **code sample** or an **executable test case** demonstrating the expected behavior that is not occurring.

* If you need help with the process Feel free to contact the author via email at john@asplinsolutions.co.uk
