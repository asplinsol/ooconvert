![npm](https://img.shields.io/npm/v/ooconvert)
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/asplinsol/ooconvert)
![npm](https://img.shields.io/npm/dm/ooconvert)
![NPM](https://img.shields.io/npm/l/ooconvert)

OOconvert
==========

**Convert Libreoffice documents into any format supported** (PDF, DOCX, XLSX, ODT, HTML etc ...)

This module was based on the conversion code that is part of the excellent [Carbone.io](Carbone.io) templating system.  
Completely rewritten in Typescript, ooconvert controls a configurable pool of python worker processes that interface with a local installation of libreoffice.


## Prerequisites

- [Libre Office](https://www.libreoffice.org/)  
  For the conversion process the library requires an installation of Libre Office (tested with v6.3.2).  
  Free installation is available at their website [www.libreoffice.org](https://www.libreoffice.org/)   

- [python](https://www.python.org/)   
  IMPORTANT! Detects and uses the python installation embedded in Libre Office. May work with external python **but not tried yet**
  
- [NodeJS](https://nodejs.org/en/)

- Tested on Linux and Windows

## Installation

`npm install ooconvert --save`

## Example Usage

The format used in the examples below is `writer_pdf_Export` which converts the document into a pdf. 

The library supports all the filters of libre office.  For a (probably out of date) listing please see the page [here](https://wiki.openoffice.org/wiki/Framework/Article/Filter/FilterList_OOo_3_0)


#### Typescript 
Note: You may need to install the typings for nodejs using `npm i @types/node --save-dev`
```typescript
import { promises as fs_promises } from "fs";
import { OOConvert } from "ooconvert";

console.log(`Converting file...`);
 
const conv2 = new OOConvert({ factories: 1 });

conv2.convertFile("test.odt", 'writer_pdf_Export', '')
.then((buffer) => fs_promises.writeFile('test.pdf', buffer))
.then(() => console.log(`Done`))
.catch((err) => console.error(err))
.then(() => conv2.exit());
```

#### Javascript

```javascript
var fs = require("fs");
var OOConvert = require("ooconvert").OOConvert;

console.log(`Converting file...`);

var conv2 = new OOConvert({ factories: 1 });

conv2.convertFile("test.odt", 'writer_pdf_Export', '')
.then((buffer) => new Promise((resolve, reject) => fs.writeFile("test.pdf", buffer, (err) => {
  if (err) reject(err);
  resolve();
})))
.then(() => console.log("Done"))
.catch((err) => console.error(err))
.then(() => conv2.exit())
```

## API Reference


#### convertFile(inputFile, outputType, formatOptions, outputFilepath)

- inputFile      `<string>`: path to the file you wish to convert
- outputType     `<string>`: name of the [Libre Office filter](https://wiki.openoffice.org/wiki/Framework/Article/Filter/FilterList_OOo_3_0) to use for conversion
- formatOptions  `<string>`: optional parameter for additional options specific to the chosen filter
- outputFilepath `<string>`: optional parameter path to specify the full path to the destination to save the result. Defaults to temp random name in the user's temp directory

## Running the tests

The library uses mocha and chai for unit testing. To run the tests is as simple as running  
`npm test`

The library can output debug console messages thanks to the [debug](https://www.npmjs.com/package/debug) library. To use simply set the DEBUG environment variable:

##### On Windows
      set DEBUG=ooconv
##### On Linux
      export DEBUG=ooconv

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/asplinsol/ooconvert/blob/master/CONTRIBUTING.md) for details and the process for submitting pull requests.

## Versioning

[SemVer](http://semver.org/) is used for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/asplinsol/ooconvert/tags). 

## Contributors

* **John Asplin** - *Author* - [asplinsol](https://gitlab.com/asplinsol)
