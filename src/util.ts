import { dirname, join, resolve, normalize, isAbsolute } from 'path';
import { readdirSync, readlinkSync } from 'fs';

import { sync } from 'which';

import { debug as _debug } from 'debug';
const debug = _debug('ooconvert/utils');
/**
 * Detect If LibreOffice and python are available at startup
 */
function _findBundledPython(sofficePath, pythonName): string {
    if (!sofficePath) {
        return null;
    }
    // Try finding a Python binary shipped alongside the soffice binary,
    //  either in its actual directory, or - if it's a symbolic link -
    //  in the directory it points to.
    let _symlinkDestination: string;
    let sofficeActualDirectory: string;
    try {
        _symlinkDestination = resolve(dirname(sofficePath), readlinkSync(sofficePath));
        // Assume symbolic link, will throw in case it's not:
        sofficeActualDirectory = dirname(_symlinkDestination);
    } catch (errorToIgnore) {
        // Not a symlink.
        sofficeActualDirectory = dirname(sofficePath);
    }
    // Check for the Python binary in the actual soffice path:
    try {
        return sync(pythonName, { path: sofficeActualDirectory });
    } catch (errorToIgnore) {
        // No bundled Python found.
        return null;
    }
}

export function _findBinaries(paths: string[]): { soffice: string; python: string } {
    // const pythonName = 'python';
    const sofficeName = 'soffice';
    // Look for the soffice binary - first in the well-known paths, then in
    //  the system PATH. On Linux, this prioritizes "upstream" (TDF) packages
    //  over distro-provided ones from the OS' repository.
    const _whichSoffice =
        sync(sofficeName, { path: paths.join(':'), nothrow: true }) || sync(sofficeName, { nothrow: true }) || null;
    // Check for a Python binary bundled with soffice, fall back to system-wide:
    // This is a bit more complex, since we deal with some corner cases.
    // 1. Hopefully use the python from the original soffice package, same dir
    //  (this might fail on Mac if python is not in MacOS/, but in Resources/).
    // 1a. Corner case: on Linux, if soffice was in /usr/bin/soffice and NOT
    //  a symlink, then we would hit /usr/bin/python, which is probably python2.
    //  This is why we try with python3 first, to defend against this.
    // 2. Try finding it in any of the well-known paths - this might result in
    //  using Python from *another install* of LibreOffice, but it should be ok.
    //  This is only attempted if the paths exist on this system to avoid
    //  a fallback to system PATH that "which" does when passed an empty string.
    // 3. Fall back to system python (hopefully named python3).
    const _whichPython =
        _findBundledPython(_whichSoffice, 'python3') ||
        _findBundledPython(_whichSoffice, 'python') ||
        (paths.length > 0 && sync('python3', { path: paths.join(':'), nothrow: true })) ||
        (paths.length > 0 && sync('python', { path: paths.join(':'), nothrow: true })) ||
        sync('python3', { nothrow: true }) ||
        sync('python', { nothrow: true }) ||
        null;
    return {
        soffice: _whichSoffice,
        python: _whichPython,
    };
}

function _listProgramDirectories(basePath, pattern): string[] {
    try {
        return readdirSync(basePath)
            .filter(dirname => pattern.test(dirname))
            .map(dirname => join(basePath, dirname, 'program'));
    } catch (errorToIgnore) {
        return [];
    }
}

export function convertToURL(inputPath: string): string {
    // Guard clause: if it already looks like a URL, keep it that way.
    if (inputPath.slice(0, 8) === 'file:///') {
        return inputPath;
    }
    if (!isAbsolute(inputPath)) {
        throw new Error('Paths to convert must be absolute');
    }
    // Split into parts so that we can join into a URL:
    const _normalizedPath = normalize(inputPath);
    // (Use both delimiters blindly - we're aiming for maximum compatibility)
    const _pathComponents = _normalizedPath.split(/[\\/]/);
    // Make sure there is no leading empty element, since we always add a
    //  leading "/" anyway.
    if (_pathComponents[0] === '') {
        _pathComponents.shift();
    }
    const outputURL = 'file:///' + _pathComponents.join('/');
    return outputURL;
}

export function getPaths(): string[] {
    // overridable file names to look for in the checked paths:
    const _linuxDirnamePattern = /^libreoffice\d+\.\d+$/;
    const _windowsDirnamePattern = /^LibreOffice ?\d*(?:\.\d+)?$/i;
    let _pathsToCheck = [];

    if (process.platform === 'darwin') {
        _pathsToCheck = _pathsToCheck.concat([
            // It is better to use the python bundled with LibreOffice:
            '/Applications/LibreOffice.app/Contents/MacOS',
            '/Applications/LibreOffice.app/Contents/Resources',
        ]);
    } else if (process.platform === 'linux') {
        // The Document Foundation packages (.debs, at least) install to /opt,
        //  into a directory named after the contained LibreOffice version.
        // Add any existing directories that match this to the list.
        _pathsToCheck = _pathsToCheck.concat(_listProgramDirectories('/opt', _linuxDirnamePattern));
    } else if (process.platform === 'win32') {
        _pathsToCheck = _pathsToCheck
            .concat(_listProgramDirectories('C:\\Program Files', _windowsDirnamePattern))
            .concat(_listProgramDirectories('C:\\Program Files (x86)', _windowsDirnamePattern));
    } else {
        debug('your platform "%s" is not supported yet', process.platform);
    }
    return _pathsToCheck;
}
