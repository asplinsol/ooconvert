import { join } from 'path';
import { readFile, remove, unlink } from 'fs-extra';
import { v4 as uuid } from 'uuid';
import { spawn, ChildProcessWithoutNullStreams } from 'child_process';
import { debug as _debug } from 'debug';
import { convertToURL, _findBinaries, getPaths } from './util';
import { tmpdir } from 'os';

const debug = _debug('ooconvert');

const PYTHON_FILE = join(__dirname, 'converter.py');

const converterOptions = {
    /* Python path */
    pythonExecPath: 'python',
    /* Libre Office executable path */
    sofficeExecPath: 'soffice',
    /* Delay before killing the other process (either LibreOffice or Python) when one of them died */
    delayBeforeKill: 500,
};

enum pythonErrors {
    'Global error' = 1,
    'Existing office server not found' = 100,
    'Could not open document' = 400,
    'Could not convert document' = 401,
}

interface Factory {
    mode: 'pipe';
    pipeName: string;
    isReady: boolean;
    userCachePath: string;
    pythonThread: ChildProcessWithoutNullStreams;
    officeThread: ChildProcessWithoutNullStreams;
    isConverting: boolean;
    exitCallback?: () => void;
    currentJob?: Job;
    readyCallback?: (newFactory?: Factory) => void;
}
interface Job {
    nbAttempt: number;
    outputFormat: string;
    inputFilePath: string;
    outputFilePath: string;
    formatOptions: string;
    callback(error: string, content?: Buffer);
    error?: string;
}

enum CONVERTER_STATUS {
    STARTING,
    READY,
    SHUTTINGDOWN,
    SHUTDOWN,
}

type FactoriesReadyCallback = (error?: string) => void;

export class OOConvert {
    isLibreOfficeFound = false;
    isPythonFound = false;
    maxFactories = 4;
    maxAttempts = 2;
    pipeNamePrefix = 'pipe_';
    /* Factories object */
    private conversionFactories: Array<Factory> = new Array<Factory>();
    /* An active factory is a factory which is starting (but not started completely), running or stopping (but not stopped completely) */
    //private activeFactories = new Array<String>();
    /* Every conversion is placed in this job queue */
    private jobQueue = new Array<Job>();
    /* If true, a factory will restart automatically */
    private isAutoRestartActive = true;

    private status: CONVERTER_STATUS = CONVERTER_STATUS.SHUTDOWN;

    private readyCallback: (error?: string) => void;

    /**
     * Initialize the converter.
     * @param {Object}   options : same options as carbone's options
     * @param {function} callback(): called when all factories are ready.
     */
    constructor(
        options?: { startFactory?: boolean; factories: number } | FactoriesReadyCallback,
        callback?: (error?: string) => void,
    ) {
        // Common logic for all OSes: perform the search and save results as options:
        const _foundPaths = _findBinaries(getPaths());
        if (_foundPaths.soffice) {
            debug('LibreOffice found at %s', _foundPaths.soffice);
            this.isLibreOfficeFound = true;
            converterOptions.sofficeExecPath = _foundPaths.soffice;
        }
        if (_foundPaths.python) {
            debug('Python found at %s', _foundPaths.python);
            this.isPythonFound = true;
            converterOptions.pythonExecPath = _foundPaths.python;
        }
        if (options) {
            if (typeof options !== 'function') {
                const opts = options as { startFactory?: boolean; factories: number };
                this.readyCallback = callback;
                if (opts && opts.factories) {
                    this.maxFactories = opts.factories;
                }
                if (opts && opts.startFactory === false) {
                    return; //skip starting factory
                }
            }
        }

        if (this.isLibreOfficeFound === false) {
            debug('cannot find LibreOffice. Document conversion cannot be used');
            return;
        }
        this.status = CONVERTER_STATUS.STARTING;
        for (let i = 0; i < this.maxFactories; i++) {
            this.addConversionFactory();
        }
        debug(`Started ${this.conversionFactories.length} factories`);
        this.readyCallback = callback || (typeof options === 'function' ? (options as FactoriesReadyCallback) : null);
    }
    getStatus(): { status: CONVERTER_STATUS; factories: { ready: number; busy: number; total: number } } {
        const total = this.conversionFactories.length;
        let ready = 0;
        let busy = 0;
        this.conversionFactories.forEach(_f => {
            if (_f.isReady) ready++;
            if (_f.isConverting) busy++;
        });
        return { status: this.status, factories: { ready, busy, total } };
    }
    busyFactories(): number {
        return this.conversionFactories.filter(_f => _f.isConverting).length;
    }
    /**
     * Kill all LibreOffice + Python threads
     * When this method is called, we must call init() to re-initialize the converter
     *
     * @param {function} callback : when everything is off
     */
    async exit(): Promise<void> {
        this.isAutoRestartActive = false;
        this.status = CONVERTER_STATUS.SHUTTINGDOWN;
        this.jobQueue.splice(0);
        debug(`Found ${this.conversionFactories.length} factories to exit`);
        await Promise.all(
            this.conversionFactories.map(_f => {
                return new Promise(resolve =>
                    setTimeout(async () => {
                        resolve(
                            this.factoryShutdown(_f).catch(error =>
                                debug(`Error shutting down factory ${_f.pipeName}: ${error}`),
                            ),
                        );
                    }),
                );
            }),
        );
        return debug('exited!');
    }

    /**
     * Convert a document
     *
     * @param {string} inputFile : absolute path to the source document
     * @param {string} outputType : destination type among all types returned by getTypes
     * @param {string} formatOptions : options passed to convert
     * @returns  function(buffer) result
     */
    convertFile(
        inputFile: string,
        outputType: string,
        formatOptions: string,
        outputFilepath?: string,
    ): Promise<Buffer> {
        return new Promise((resolve, reject) => {
            if (this.isLibreOfficeFound === false) {
                return reject('Cannot find LibreOffice. Document conversion cannot be used');
            }
            const callback = (error: string, content?: Buffer): void => {
                if (error) {
                    reject(new Error(error));
                } else {
                    resolve(content);
                }
            };
            const _output = uuid();
            const _job = {
                inputFilePath: inputFile,
                outputFilePath: outputFilepath || join(tmpdir(), _output),
                outputFormat: outputType,
                formatOptions: formatOptions || '',
                callback,
                nbAttempt: 0,
                error: null,
            };
            this.jobQueue.push(_job);
            setTimeout(() => this.executeQueue(), 100);
        });
    }

    /**
     * Add a LibreOffice + Python factory (= 2 threads)
     * @param {function} callback : function() called when the factory has been created.
     */
    private addConversionFactory(): Promise<Factory> {
        let _userCachePath: string;
        // maximum of factories reached
        if (this.conversionFactories.length === this.maxFactories) {
            const failedFactory = this.conversionFactories.find(_factory => !_factory.isReady);
            if (failedFactory) {
                // re-use previous directory if possible (faster restart)
                _userCachePath = failedFactory.userCachePath;
                this.factoryShutdown(failedFactory);
            } else {
                return Promise.reject(`Not able to add new factory - max factories limit reached`); //refuse to create a new factory
            }
        }
        const _uniqueName = uuid();
        // generate a unique path to a fake user profile. We cannot start multiple instances of LibreOffice if it uses the same user cache
        _userCachePath = _userCachePath || join(tmpdir(), '_office_' + _uniqueName);
        // generate a unique pipe name
        const _pipeName = this.pipeNamePrefix + '_' + _uniqueName;
        const _factory: Factory = {
            mode: 'pipe',
            pipeName: _pipeName,
            userCachePath: _userCachePath,
            officeThread: null,
            pythonThread: null,
            isReady: false,
            isConverting: false,
        };
        if (this.spawnOffice(_factory).officeThread && this.spawnPython(_factory).pythonThread) {
            this.conversionFactories.push(_factory);
            debug(`New factory created with pipeName ${_factory.pipeName}`);
            return new Promise(resolve => setTimeout(() => resolve(_factory), 50));
        } else {
            throw new Error('Cannot start LibreOffice or Python Thread'); //should be redundant
        }
    }

    private spawnOffice(_factory: Factory): Factory {
        const _connectionString = 'pipe,name=' + _factory.pipeName + ';urp;StarOffice.ComponentContext';
        // generate a URL in LibreOffice's format so that it's portable across OSes:
        // see: https://wiki.openoffice.org/wiki/URL_Basics
        const _userCacheURL = convertToURL(_factory.userCachePath);
        const _officeParams = [
            '--headless',
            '--invisible',
            '--nocrashreport',
            '--nodefault',
            '--nologo',
            '--nofirststartwizard',
            '--norestore',
            '--quickstart',
            '--nolockcheck',
            '--accept=' + _connectionString,
            '-env:UserInstallation=' + _userCacheURL,
        ];
        const _officeThread = spawn(converterOptions.sofficeExecPath, _officeParams);
        if (_officeThread) {
            _officeThread.on('close', (code, signal) => {
                debug(`Office thread on factory ${_factory.pipeName} closing with code ${code} and signal ${signal}`);
                if (_factory.officeThread) {
                    _factory.officeThread = null;
                    this.factoryShutdown(_factory);
                }
            });
            _factory.officeThread = _officeThread;
        } else {
            throw new Error('Cannot start office Thread');
        }
        return _factory;
    }
    private spawnPython(_factory: Factory): Factory {
        const _pythonThread = spawn(converterOptions.pythonExecPath, [PYTHON_FILE, '--pipe', _factory.pipeName]);
        if (_pythonThread) {
            _pythonThread.on('close', (code, signal) => {
                debug(`Python thread on factory  ${_factory.pipeName} closing with code ${code} and signal ${signal}`);
                if (_factory.pythonThread) {
                    _factory.pythonThread = null;
                    this.factoryShutdown(_factory);
                }
            });
            _pythonThread.stdout.on('data', data => this.onNewMessage(_factory, data.toString()));
            _pythonThread.stderr.on('data', function(err) {
                debug('python stderr :', err.toString());
                const _job = _factory.currentJob;
                if (_job) {
                    _job.callback(err.toString());
                    _factory.currentJob = null;
                }
            });
            _factory.pythonThread = _pythonThread;
        } else {
            throw new Error('Cannot start Python Thread');
        }
        return _factory;
    }

    private factoryShutdown(_factory: Factory): Promise<Factory> {
        return new Promise(resolve => {
            debug(`shutting down factory ${_factory.pipeName} `);
            // remove factory from activeFactories to avoid infinite loop
            const index = this.conversionFactories.findIndex(_f => _f.pipeName === _factory.pipeName);
            if (index > -1) {
                debug(`Removing factory ${_factory.pipeName} from list`);
                this.conversionFactories.splice(index, 1);
            }
            // the factory cannot receive jobs anymore
            _factory.isReady = false;
            _factory.isConverting = false;
            // kill any remaining processes
            if (_factory.pythonThread !== null) {
                _factory.pythonThread.removeAllListeners().kill('SIGKILL');
                _factory.pythonThread = null;
            }
            if (_factory.officeThread !== null) {
                _factory.officeThread.removeAllListeners().kill('SIGKILL');
                _factory.officeThread = null;
            }
            resolve(this.whenFactoryIsCompletelyOff(_factory));
        });
    }

    /**
     * Manage factory restart ot shutdown when a factory is completly off
     * @param  {Object} factory factory description
     */
    private whenFactoryIsCompletelyOff(factory: Factory): Promise<Factory> {
        return new Promise(async resolve => {
            // if Carbone is not shutting down
            if (this.isAutoRestartActive === true) {
                // if there is an error while converting a document, let's try another time
                const _job = factory.currentJob;
                if (_job) {
                    if (_job.nbAttempt < this.maxAttempts) {
                        debug(`Rescheduling current job from failed factory ${factory.pipeName} `);
                        factory.currentJob = null;
                        this.jobQueue.push(_job);
                    } else {
                        _job.callback('Could not convert the document', null);
                    }
                }
                const newFactory = await this.addConversionFactory();
                setTimeout(() => this.executeQueue(), 100);
                resolve(newFactory);
            }
            // else if Carbone is shutting down and there is an exitCallback
            else {
                // delete office files synchronously (we do not care because Carbone is shutting down) when office is dead
                await remove(factory.userCachePath);
                debug(`Factory ${factory.pipeName} shut down`);
                return resolve(factory);
            }
        });
    }
    private onNewMessage(_factory: Factory, msg: string): void {
        debug(`msg (${_factory.pipeName}): ${msg}`);
        switch (msg) {
            case '204':
                return this.onReady(_factory);
            case '254':
                process.exit(1);
            case '200':
                return this.onComplete(_factory, msg);
            default:
                return this.onComplete(_factory, msg);
        }
        // setTimeout(() =>  this.executeQueue()), 50;
    }
    private onReady(_factory: Factory): void {
        // Ready to receive conversion
        debug('factory ' + _factory.pipeName + ' ready');
        _factory.isReady = true;
        if (_factory.readyCallback && this.getStatus().factories.ready === this.getStatus().factories.total) {
            setTimeout(() => _factory.readyCallback(), 50);
        }
        setTimeout(() => this.executeQueue()), 0;
    }
    private onComplete(_factory: Factory, msg: string): void {
        debug(`result from factory ${_factory.pipeName} : ${msg}`);
        // Document converted
        const _job = _factory.currentJob;
        _factory.currentJob = null;
        _factory.isConverting = false;
        if (_job) {
            _job.error = pythonErrors[msg] !== undefined ? pythonErrors[msg] : null;
            if (_job.error) {
                if (_job.nbAttempt < this.maxAttempts) {
                    debug(_job.error + ': attempt ' + _job.nbAttempt, _job);
                    _job.error = null;
                    this.jobQueue.push(_job);
                } else {
                    _job.callback(_job.error);
                }
                this.executeQueue();
            } else {
                readFile(_job.outputFilePath, function(err, content) {
                    _job.callback((err && err.message) || undefined, content);
                    unlink(_job.outputFilePath, function(err) {
                        if (err) {
                            debug('cannot remove file' + _job.outputFilePath);
                        }
                    });
                });
            }
        }
    }
    /**
     * Execute the queue of conversion.
     * It will auto executes itself until the queue is empty
     */
    private executeQueue(): void {
        if (this.jobQueue.length === 0) {
            return;
        }
        debug(`Processing queue (length ${this.jobQueue.length})`);
        for (const _factory of this.conversionFactories) {
            if (this.jobQueue.length > 0) {
                if (_factory.isReady === true && _factory.isConverting === false) {
                    const _job = this.jobQueue.shift();
                    this.sendToFactory(_factory, _job);
                }
            }
        }
    }

    /**
     * Send the document to the Factory
     *
     * @param {object} factory : LibreOffice + Python factory to send to
     * @param {object} job : job description (file to convert, callback to call when finished, ...)
     */
    private sendToFactory(factory: Factory, j: Job): void {
        debug(`Sending job to factory ${factory.pipeName}`);
        factory.isConverting = true;
        factory.currentJob = j;
        factory.pythonThread.stdin.write(
            '--format="' +
                j.outputFormat +
                '" --input="' +
                j.inputFilePath +
                '" --output="' +
                j.outputFilePath +
                '" --formatOptions="' +
                j.formatOptions +
                '"\n',
        );
        // keep the number of attempts to convert this file
        j.nbAttempt++;
    }
}
