import { OOConvert } from './converter';
import * as path from 'path';
import { describe } from 'mocha';
import { expect } from 'chai';

import { statSync } from 'fs-extra';

describe(`ODT Conversion library`, function() {
    const FACTORYCOUNT = 2;
    let conv: OOConvert;
    describe(`Setup`, function() {
        it(`conversion client should not be null`, function() {
            conv = new OOConvert({ factories: FACTORYCOUNT });
            return expect(conv).exist;
        });
        it(`Python path detected`, function() {
            return expect(conv.isPythonFound).is.true;
        });
        it(`Office path detected`, function() {
            return expect(conv.isLibreOfficeFound).is.true;
        });
        it(`all factories created`, function() {
            return expect(conv.getStatus().factories.total).is.eq(FACTORYCOUNT);
        });
        it(`all factories ready`, function(done) {
            this.timeout(0);
            (function waitLoop(i): void {
                setTimeout(function() {
                    if (conv.getStatus().factories.ready === FACTORYCOUNT) {
                        done();
                    } else if (--i) {
                        waitLoop(i);
                    } else {
                        done(new Error('timeout waiting for factories to be ready '));
                    }
                }, 1000);
            })(300);
        });
    });
    describe('Usage', function() {
        const TESTFILE = path.join(__dirname, '..', 'test-data', 'test.odt');
        it(`Test file exists`, function() {
            expect(statSync(TESTFILE).isFile()).to.be.true;
        });
        it('converts odt to txt', async function() {
            const buffer = await conv.convertFile(TESTFILE, 'Text', '');
            expect(buffer.toString('utf8', 0, 4)).to.eq('test');
        });
        it('converts odt to pdf', async function() {
            const buffer = await conv.convertFile(TESTFILE, 'writer_pdf_Export', '');
            expect(buffer.toString('utf8', 0, 4)).to.eq('%PDF');
        });
        it('converts odt to docx', async function() {
            const buffer = await conv.convertFile(TESTFILE, 'MS Word 2007 XML', '');
            expect(buffer.toString('utf8', 0, 4)).to.eq('\x50\x4b\x03\x04'); //zip header
        });
        it(`waits for factory ready when calling convert function`, async function() {
            this.timeout(0);
            const conv2 = new OOConvert({ factories: 1 });
            const buffer = await conv2.convertFile(TESTFILE, 'writer_pdf_Export', '');
            expect(buffer.toString('utf8', 0, 4)).to.eq('%PDF');
            await conv2.exit();
        });
    });
    describe(`shutdown`, function() {
        it(`all factories removed`, async function() {
            await conv.exit();
            expect(conv.getStatus().factories.total).is.eq(0);
        });
    });
});
